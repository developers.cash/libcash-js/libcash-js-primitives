# LibCash-JS Primitives

Declarative OO wrapper around LibAuth Primitives.

Each primiive (with the exception of `Transaction`) will be immutable:

1. The classes are instantiated either from a constructor or `from...()` static method.
2. Which stores the base primitive in a `readonly` member variable.
3. Which can then be transformed and returned using other members of the class (e.g. `toHex()`, `toCashAddr()`, `signMessageHashCompact()`, etc).

## Some Examples

Address:

```ts
// Read address.
const address = Address.fromCashAddrOrLegacy('1JKKPnWCJctEn1ZAyNzwCQQuDEWc6M7ZK8')

// Output as Cash Address
console.log(address.toCashAddr()

// Output as Legacy Address
console.log(address.toLegacy()
```

Private Key:

```ts
// Get private key from address.
const privateKey = PrivateKey.fromWif('KyerGhLWTejDqwKNQtZrzBf1xtRcdmMdiUwgy6DCWjzgWN4hSmDM')

// Derive public key.
const publicKey = privateKey.derivePublicKey()

// Derive address.
const addresss = publicKey.deriveAddress()

// Output Address in Legacy Format.
console.log(address.toLegacy()
```

Units:

```ts
// Convert BCH to mBCHl
const mBCH = Units.fromBCH(1).toMBCH();

console.log(mBCH);
```

Script:

```ts
// Convert hex to lockscript.
const script = Script.fromHex(
  '76a9145ca0500d8d8c3d769946252c3f4409696fa5207488ac',
);

// Print as assembly.
console.log(script.toASM());

// Get address from lockscript.
const address = Address.fromLockscript(script.toBytes());

// Print the address in CashAddr format.
console.log(address.toCashAddr());
```

Script:

```ts
// Convert hex to lockscript.
const scriptFromASM = Script.fromASM(
  'OP_DUP OP_HASH160 OP_PUSHBYTES_20 5ca0500d8d8c3d769946252c3f4409696fa52074 OP_EQUALVERIFY OP_CHECKSIG',
);

// Get address from lockscript.
const address = Address.fromLockscriptHex(scriptFromASM.toHex());

// Print the address in CashAddr format.
console.log(address.toCashAddr());
```

Transaction Hash:

```ts
// Convert hex to lockscript.
const transactionHash = TransactionHash.fromTransactionHex(
  '0200000002ea09d1f0696a6f8aeeec21e15bdb6fdf8060995b4abefa361fc2dc3867eebfef0000000064419df009ecd9fea89343a5e190fa9af0ccce303c5412244bd580094ffc328fc55a26c1c48001cf290580ed6b25381a2cfa26ca40b5d323f4f4637ed213829be38ac121028b713d865d13156754a7085fe6b0c2db507fa977990db986b7737a045ab56ea0ffffffff5860d3520a83594ac8f70ac2da000ca605a23edc1165bc1d0733319e678bb4c6000000006441f588ac4ceeeb3984e3784b6eb43f1061086285dc4ce3c25c5e9398d113d47b6e70662884768c69d3d573b3cd1bf1b29e8824451491c580e9a884312868bcd823c12103551dd2a6e60143ec31267f910ec34ac64c2ab6c9bbdf09f7d8685f500d8eda50ffffffff02b3962c000000000017a9148c108385c434a23c14f238387336ba7e31a93534879e0c0000000000001976a9143c450d8bf58c22984cfb1dbd40d4e02ed1e94e9588ac00000000',
);

// Print the Hash as hex.
console.log(transactionHash.toHex());
```

## Testing

I haven't worked out how to do this in code yet with Ava but, as each primitive is stored the same way, we can:

```ts
foreach(instnntiationMethod of instantiationMethods)
{
	testOtherMethodsWithEachInstantiationMethod(instnntiationMethod)
}
```

Alternatively, we could check the primitive in the class itself, but I don't really like this.

Making tests inspect internal state feels like a maintenance burden.
