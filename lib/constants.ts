// Consensus constants.
export const SATOSHI_DUST_LIMIT = 546;

// P2P Consensus.
export const MAX_TRANSACTION_SIZE = 1;
