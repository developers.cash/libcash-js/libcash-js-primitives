import { Address } from './address.js';
import { Script } from './script.js';
import { PrivateKey } from './private-key.js';
import { TransactionHash } from './transaction-hash';

import { SATOSHI_DUST_LIMIT } from './constants.js';

import type { Input, Output, TransactionBCH } from '@bitauth/libauth';

import {
	Opcodes,
	binToHex,
	hexToBin,
	cashAddressToLockingBytecode,
	encodeDataPush,
	encodeTransaction,
	decodeTransaction,
	flattenBinArray,
	generateSigningSerializationBCH,
	isHex,
	sha256,
	secp256k1,
} from '@bitauth/libauth';

const SIGHASH_ALL = 0x41;

export type ScriptTypes = Uint8Array | Address | string;

/**
* Abstract class for Unlocking Script Builders.
*/
export abstract class Unlocker
{
	abstract unlock(transaction: TransactionBCH, inputIndex: number, unlockers: Array<Unlocker>): Uint8Array;

	abstract getOutput(): Output;
}

export class UnlockCustom extends Unlocker
{
	private lockScript: Uint8Array;
	private unlockScript: Uint8Array;
	private satoshis: number;

	constructor(lockScript: Uint8Array, unlockScript: Uint8Array, satoshis: number)
	{
		super();

		this.lockScript = lockScript;
		this.unlockScript = unlockScript;
		this.satoshis = satoshis;
	}

	public unlock(transaction: TransactionBCH, inputIndex: number, unlockers: Array<Unlocker>): Uint8Array
	{
		return this.unlockScript;
	}

	public getOutput(): Output
	{
		return {
			lockingBytecode: this.lockScript,
			valueSatoshis: BigInt(this.satoshis),
		}
	}
}

/**
* P2PKH Unlocker.
*/
export class UnlockP2PKH extends Unlocker
{
	private privateKey: PrivateKey;
	private satoshis: number;
	private sigHash: number;

	/**
	* Creates a new P2PKH Input Unlocker
	*
	* @param privateKey {PrivateKey} The Private Key to use to unlock this P2PKH input.
	* @param satoshis   {number}     The number of Satoshis locked into this input.
	* @param sigHash    {number}     The signature hashing algorithm to use.
	*/
	constructor(privateKey: PrivateKey, satoshis: number, sigHash: number = SIGHASH_ALL)
	{
		super();

		this.privateKey = privateKey;
		this.satoshis = satoshis;
		this.sigHash = sigHash;
	}

	public unlock(transaction: TransactionBCH, inputIndex: number, unlockers: Array<Unlocker>): Uint8Array
	{
		// Create signing serialization.
		const signingSerialization = this.createSigningSerialization(transaction, inputIndex, unlockers);

		// Calculate signature hash.
		const sigHash = this.getSigningSerializationHash(signingSerialization);

		// Generate a signature over the "sighash" using the passed private key.
		const signMessageResult = secp256k1.signMessageHashSchnorr(this.privateKey.toBytes(), sigHash);

		// If signatureBin returns a string, it indicates an error.
		if(typeof signMessageResult === 'string')
		{
			throw(new Error(signMessageResult));
		}

		// Append the hashtype to the signature to turn it into a valid transaction signature.
		const transactionSignature = Uint8Array.from([ ...signMessageResult, this.sigHash ]);

		// Derive the public key.
		const publicKey = this.privateKey.derivePublicKey();

		// Build the unlocking script that unlocks the P2PKH locking script.
		const unlockingBytecode = flattenBinArray([ encodeDataPush(transactionSignature), encodeDataPush(publicKey.toBytes()) ]);

		// Return the unlocking bytecode.
		return unlockingBytecode;
	}

	public getOutput(): Output
	{
		const address = this.privateKey.derivePublicKey().deriveAddress();

		const script = Script.fromCashAddr(address.toCashAddr());

		return {
			lockingBytecode: script.toBytes(),
			valueSatoshis: BigInt(this.satoshis),
		};
	}

	private createSigningSerialization(transaction: TransactionBCH, inputIndex: number, unlockers: Array<Unlocker>): Uint8Array
	{
		// /home/jimtendo/Projects/Sandbox/libauth2/src/lib/vm/instruction-sets/common/signing-serialization.spec.ts

		const address = this.privateKey.derivePublicKey().deriveAddress();

		const script = Script.fromCashAddr(address.toCashAddr());

		const signingSerialization = generateSigningSerializationBCH({
			inputIndex: inputIndex,
			sourceOutputs: unlockers.map((unlocker) => unlocker.getOutput()),
			transaction,
		},
		{
			coveredBytecode: new Uint8Array(script.toBytes()),
			signingSerializationType: new Uint8Array([ this.sigHash ]),
		});

		return signingSerialization;
	}

	private getSigningSerializationHash(signingSerialization: Uint8Array): Uint8Array
	{
		const sighash = sha256.hash(sha256.hash(signingSerialization));

		return sighash;
	}
}

/**
* Transaction Entity.
*/
export class Transaction
{
	private inputs: Array<Input> = [];
	private outputs: Array<Output> = [];
	private locktime: number = 0;

	private unlockers: Array<Unlocker> = [];

	constructor(transaction?: TransactionBCH)
	{
		if(transaction)
		{
			this.inputs = transaction.inputs;
			this.outputs = transaction.outputs;
			this.locktime = transaction.locktime
		}
	}

	/**
	* Add an output to this transaction.
	*
	* @param script   {ScriptTypes}     The output (locking) script.
	* @param satoshis {bigint | number} The number of satoshis to lock into this output.
	*
	* @return {Transaction} The current transaction.
	*/
	public addOutput(script: ScriptTypes, satoshis: bigint | number): Transaction
	{
		// Convert the satoshis to bigint.
		if(typeof satoshis === 'number')
		{
			satoshis = BigInt(satoshis);
		}

		// Convert the locking script to locking bytecode.
		const lockingBytecode = this.scriptToLockingBytecode(script);

		// If it is not a data output, it must be above the satoshi dust limit.
		if(lockingBytecode[0] !== Opcodes.OP_RETURN && satoshis < SATOSHI_DUST_LIMIT)
		{
			throw(new Error(`Output satoshis (${satoshis}) is less than the Satoshi Dust Limit (${SATOSHI_DUST_LIMIT}).`));
		}

		// Add it to the list of outputs.
		this.outputs.push({
			lockingBytecode,
			valueSatoshis: satoshis,
		});

		return this;
	}

	/**
	* Add an input to this transaction.
	*
	* @param input    {Input}    The input to use.
	* @param unlocker {Unlocker} The unlocker to use for this input (e.g. UnlockP2PKH)..
	*
	* @return {Transaction} The current transaction.
	*/
	public addInput(input: Input, unlocker: Unlocker): Transaction
	{
		this.inputs.push(input);
		this.unlockers.push(unlocker);

		return this;
	}

	/**
	* Builds the transaction.
	*
	* TODO: Take into account fee somehow.
	*
	* @return {Uint8Array} The transaction bytecode.
	*/
	public build(): Uint8Array
	{
		// Create a transaction object.
		const transaction = {
			version: 2,
			inputs: this.inputs,
			outputs: this.outputs,
			locktime: this.locktime,
		} as TransactionBCH;

		// Iterate through each input and sign it.
		for(const inputIndex in transaction.inputs)
		{
			const input = transaction.inputs[inputIndex];
			const unlocker = this.unlockers[inputIndex];

			// If we have an unlocker for this input...
			if(unlocker)
			{
				input.unlockingBytecode = unlocker.unlock(transaction, Number(inputIndex), this.unlockers);
			}
		}

		// Encode the transaction.
		const encodedTransaction = encodeTransaction(transaction);

		// Return the encoded transaction.
		return encodedTransaction;
	}

	public toBytes(): Uint8Array
	{
		const bytes = this.build();

		return bytes;
	}

	public toHex(): string
	{
		const bytes = this.build();

		return binToHex(bytes);
	}

	public hash(): TransactionHash
	{
		const bytes = this.toBytes();

		const transactionHash = TransactionHash.fromTransactionBytes(bytes);

		return transactionHash;
	}

	/**
	* Create a Transaction Entity from the given byte data.
	*
	* @param bytes {Uint8Array} The raw byte data of the transaction.
	*
	* @throws {Error} If transaction data cannot be decoded.
	*
	* @returns {Transaction} The created Transaction Entity.
	*/
	public static fromBytes(bytes: Uint8Array): Transaction
	{
		// Attempt to decode the raw transaction bytes.
		const decodeResult = decodeTransaction(bytes);

		// If a string is returned, this indicates an error...
		if(typeof decodeResult === 'string')
		{
			throw(new Error(decodeResult));
		}

		// Return a new instance of the Transaction Entity created from the decode result.
		return new Transaction(decodeResult);
	}

	/**
	* Create a Transaction Entity from the given byte data encoded as hexadecimal string.
	*
	* @param hex {string} The raw byte data (encoded as hexadecimal string) of the transaction.
	*
	* @throws {Error} If transaction cannot be decoded.
	*
	* @returns {Transaction} The created Transaction Entity.
	*/
	public static fromHex(hex: string): Transaction
	{
		// Ensure that the given string is hex.
		if(!isHex(hex))
		{
			throw(new Error('The given string is not valid hexadecimal.'));
		}

		// Convert our hex to binary.
		const bytes = hexToBin(hex);

		// Return the transaction constructed from our bytes.
		return Transaction.fromBytes(bytes);
	}

	private scriptToLockingBytecode(lockingScript: ScriptTypes): Uint8Array
	{
		// If Locking Script is a string, try to convert it to an address.
		if(typeof lockingScript === 'string')
		{
			lockingScript = Address.fromCashAddrOrLegacy(lockingScript);
		}

		// If it is now an address, convert it to bytecode.
		if(lockingScript instanceof Address)
		{
			const result = cashAddressToLockingBytecode(lockingScript.toCashAddr());

			if(typeof result === 'string')
			{
				throw(new Error(result));
			}

			lockingScript = result.bytecode;
		}

		// Make sure we ended up with a Uint8Array.
		if(!(lockingScript instanceof Uint8Array))
		{
			throw(new Error('Could not encode locking script.'));
		}

		return lockingScript;
	}
}
