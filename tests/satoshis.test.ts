import test from 'ava';
import { Satoshis } from '../lib/satoshis.js';

test('toSatoshis', (t) => {
  const sats = new Satoshis(100_000_000);
  t.is(sats.toSatoshis(), 100_000_000);
});

test('toBits', (t) => {
  const sats = new Satoshis(100_000_000);
  t.is(sats.toBits(), 1_000_000);
});

test('toMBCH', (t) => {
  const sats = new Satoshis(100_000_000);
  t.is(sats.toMBCH(), 1_000);
});

test('toBCH', (t) => {
  const sats = new Satoshis(100_000_000);
  t.is(sats.toBCH(), 1);
});

test('fromSatoshis', (t) => {
  const sats = Satoshis.fromSatoshis(100_000_000);
  t.is(sats.toSatoshis(), 100_000_000);
});

test('fromBits', (t) => {
  const sats = Satoshis.fromBits(1_000_000);
  t.is(sats.toSatoshis(), 100_000_000);
});

test('fromMBCH', (t) => {
  const sats = Satoshis.fromMBCH(1000);
  t.is(sats.toSatoshis(), 100_000_000);
});

test('fromBCH', (t) => {
  const sats = Satoshis.fromBCH(1);
  t.is(sats.toSatoshis(), 100_000_000);
});

test('roundToDigits', (t) => {
  const testNumbers = [
    { input: 0.1 + 0.2, expected: 0.3, digits: 1 },
    { input: 0.1 + 0.7, expected: 0.8, digits: 1 },
    { input: 4.6 * 100_000_000, expected: 460000000, digits: 0 },
  ];

  for (const { input, digits, expected } of testNumbers) {
    t.is(Satoshis.roundToDigits(input, digits), expected);
  }
});
